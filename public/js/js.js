const dropzone = document.getElementById('dropzone');
const fileInput = document.getElementById('file-input');

['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
    dropzone.addEventListener(eventName, preventDefaults, false);
});

function preventDefaults(e) {
    e.preventDefault();
    e.stopPropagation();
}

dropzone.addEventListener('drop', handleDrop, false);
fileInput.addEventListener('change', handleInputChange, false);

function handleDrop(e) {
    handleFiles(e.dataTransfer.files);
}

function handleInputChange(e) {
    handleFiles(e.target.files);
}

const MAX_FILES = 100;
const MAX_SIZE = 100 * 1024 * 1024;
const ALLOWED_EXTENSIONS = ['jpeg', 'png', 'jpg', 'gif'];

function handleFiles(files) {
    const formData = new FormData();
    const progressBar = document.createElement('progress');
    progressBar.setAttribute('max', 100);
    progressBar.setAttribute('value', 0);
    document.getElementById('content').insertBefore(progressBar, document.getElementById('files'));

    const spinner = document.createElement('div');
    spinner.className = 'spinner-border';
    document.getElementById('content').insertBefore(spinner, document.getElementById('files'));

    let validFiles = 0;

    if (files.length > MAX_FILES) {
        showNotification('Maximum number of files exceeded.', 'error');
        document.getElementById('content').removeChild(progressBar);
        document.getElementById('content').removeChild(spinner);
        return;
    }

    for (let i = 0; i < files.length; i++) {
        const file = files[i];
        if (file.size > MAX_SIZE) {
            showNotification('Maximum file size exceeded for file: ' + file.name, 'error');
            continue;
        }

        const extension = file.name.split('.').pop().toLowerCase();
        if (!ALLOWED_EXTENSIONS.includes(extension)) {
            showNotification('File type not allowed for file: ' + file.name, 'error');
            continue;
        }

        formData.append('files[]', file);
        validFiles++;
    }

    if (validFiles === 0) {
        document.getElementById('content').removeChild(progressBar);
        document.getElementById('content').removeChild(spinner);
        return;
    }

    formData.append('_token', document.querySelector("meta[name=_token]").content);
    formData.append('quality', document.querySelector("input[name=quality]").value);

    axios.post('./compressImages', formData, {
        onUploadProgress: function (progressEvent) {
            const percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
            progressBar.setAttribute('value', percentCompleted);
        }
    })
        .then(response => {
            const data = response.data.data;
            document.getElementById('content').removeChild(progressBar);
            document.getElementById('content').removeChild(spinner);

            Object.keys(data).forEach(function (filename) {
                const file = data[filename];
                const a = document.createElement('a');
                let span = document.createElement('span');
                span.className = 'file-name';
                span.textContent = file;
                a.appendChild(span);

                span = document.createElement('span');
                span.className = 'download-icon';
                span.innerHTML = '&#x2B07;';
                a.appendChild(span);
                a.href = './download/' + filename;
                a.target = '_blank';
                a.className = 'download-link';
                document.getElementById('files').appendChild(a);
            });
            document.getElementById('download-all').classList.add('d-flex');
        })
        .catch(error => {
            showNotification('An error occurred while uploading files.', 'error');
            document.getElementById('content').removeChild(progressBar);
            document.getElementById('content').removeChild(spinner);
        });
}

function showNotification(message, type) {
    const container = document.getElementById('notification-container');
    const notification = document.createElement('div');
    notification.className = 'notification ' + type;
    notification.textContent = message;
    container.appendChild(notification);
    setTimeout(() => {
        container.removeChild(notification);
    }, 5000);
}