<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="description"
          content="Compress and optimize your JPEG, PNG, JPG, and GIF images with our free online compression tool.">
    <meta name="keywords" content="image compression, jpeg, png, jpg, gif">
    <meta name="author" content="YOTTA dev yottaa.dev">
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow">
    <link rel="canonical" href="{{url('/')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>Images Compression Service jpeg|png|jpg|gif</title>
    <link href="{{url('css/css.css')}}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script async src="https://www.googletagmanager.com/gtag/js?id={{env('GOOGLE_ANALYTICS_ID')}}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', '{{env('GOOGLE_ANALYTICS_ID')}}');
    </script>
</head>
<body>
<div id="notification-container"></div>
<div id="top" class="d-flex flex-column">
    <h1 class="text-center fw-bold">Images Compression Service jpeg|png|jpg|gif</h1>
    <div id="dropzone" class="flex-grow-1">
        <div class="d-flex flex-column gap-3">
            <p>Drag and drop files here or click to select</p>
            <input type="file" id="file-input" multiple>
            <div>
                <label for="quality">Quality (0-100):</label>
                <input type="number" id="quality" name="quality" value="80" min="0" max="100" step="10">
            </div>
        </div>
    </div>
</div>
<div class="d-flex flex-column g-3" id="content">
    <a class="btn btn-primary justify-content-center" id="download-all" href="{{url('download/all')}}">Download all as
        zip</a>
    <div class="d-flex flex-column text-center" id="files"></div>
</div>
<script src="{{url('js/js.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</body>
</html>
