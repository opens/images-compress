<?php

use App\Actions\ClearSessionFilesAction;
use App\Http\Controllers\CompressImagesController;
use App\Http\Controllers\FileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::post('/compressImages', [CompressImagesController::class, 'compressImages'])->name('compressImages');
Route::get('/download/all', [FileController::class, 'downloadAll'])->name('downloadAll');
Route::get('/download/{filename}', [FileController::class, 'download'])
    ->where('filename', '[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}\.(jpeg|png|jpg|gif)')
    ->name('download');
Route::get('/', function () {
    ClearSessionFilesAction::handle();
    return view('index');
});
