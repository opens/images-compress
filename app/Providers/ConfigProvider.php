<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ConfigProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        ini_set('max_file_uploads', 100);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('memory_limit', '200M');
        ini_set('max_input_vars', 200);
        ini_set('max_input_nesting_level', 64);
    }
}
