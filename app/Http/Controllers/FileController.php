<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class FileController extends Controller
{
    public static function downloadAll()
    {
        $files = Session::get('files');
        if (empty($files)) {
            return null;
        }
        $zip = new ZipArchive();
        $zipFileName = 'files.zip';
        $zipFilePath = storage_path('app/' . $zipFileName);
        if ($zip->open($zipFilePath, ZipArchive::CREATE) === true) {
            $counter = [];
            foreach ($files as $filename => $originalName) {
                $fileContent = Storage::disk('files')->get(Session::getId() . '/' . $filename);
                $extension = pathinfo($originalName, PATHINFO_EXTENSION);
                $baseName = pathinfo($originalName, PATHINFO_FILENAME);
                if (isset($counter[$baseName])) {
                    $counter[$baseName]++;
                    $baseName .= '_' . $counter[$baseName];
                } else {
                    $counter[$baseName] = 1;
                }
                $newName = $baseName . '.' . $extension;
                $zip->addFromString($newName, $fileContent);
            }
            $zip->close();
        }
        $headers = [
            'Content-Type' => 'application/zip',
            'Content-Disposition' => 'attachment; filename="' . $zipFileName . '"',
        ];
        return response()->download($zipFilePath, $zipFileName, $headers)->deleteFileAfterSend();
    }

    public static function download($filename)
    {
        if (empty(Session::get('files')[$filename])) {
            return null;
        }
        $file = Storage::disk('files')->get(Session::getId() . '/' . $filename);
        $headers = [
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename="' . Session::get('files')[$filename] . '"',
        ];

        return response($file, 200, $headers);
    }
}
