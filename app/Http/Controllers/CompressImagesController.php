<?php

namespace App\Http\Controllers;

use App\Actions\AddSessionFilesAction;
use App\Http\Requests\CompressImagesRequest;
use App\Service\ImageCompressService;

class CompressImagesController extends Controller
{
    public static function compressImages(CompressImagesRequest $request, ImageCompressService $service)
    {
        $validatedData = $request->validated();
        $data = $service->handle($validatedData['files'], $validatedData['quality']);
        AddSessionFilesAction::handle($data);
        return ['data' => $data];
    }
}
