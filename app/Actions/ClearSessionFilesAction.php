<?php

namespace App\Actions;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Lorisleiva\Actions\Concerns\AsAction;

class ClearSessionFilesAction
{
    use AsAction;

    public static function handle()
    {
        $files = Session::get('files');
        if (Arr::accessible($files)) {
            foreach ($files as $file => $name) {
                Storage::disk('files')->delete(Session::getId() . '/' . $file);
            }
        }
        Session::remove('files');
    }
}
