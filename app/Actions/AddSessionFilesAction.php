<?php

namespace App\Actions;

use Illuminate\Support\Facades\Session;
use Lorisleiva\Actions\Concerns\AsAction;

class AddSessionFilesAction
{
    use AsAction;

    public static function handle(array $data)
    {
        Session::put('files', collect(Session::get('files', []))->merge($data)->toArray());
        Session::save();
    }
}
