<?php

namespace App\Service;

use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;

class ProcessService
{
    public static function handle(array $arguments): bool
    {
        $process = new Process($arguments);
        $process->start();
        $process->wait();
        if (!$process->isSuccessful()) {
            Log::error(Self::class, [$process->getOutput(), $process->getErrorOutput(), $process->getCommandLine()]);
            return false;
        }
        return true;
    }
}