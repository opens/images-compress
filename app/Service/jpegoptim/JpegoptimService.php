<?php

namespace App\Service\jpegoptim;

use App\Service\ProcessService;
use Symfony\Component\HttpFoundation\File\File;

class JpegoptimService
{
    public static function handle(File $file, int $quality = 80): string|null
    {
        $arguments = [
            env('JPEGOPTIM_PATH'),
            $file->getPathName(),
            '-m',
            $quality
        ];
        return ProcessService::handle($arguments) ? $file->getFilename() : null;
    }
}