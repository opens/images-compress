<?php

namespace App\Service\pngquant;

use App\Service\ProcessService;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\File;

class PngquantService
{
    public static function handle(File $file, int $quality = 80): string|null
    {
        $outputFile = Uuid::uuid4() . '.' . $file->getExtension();
        $outputPath = storage_path('files/' . Session::getId() . '/' . $outputFile);
        $arguments = [
            env('PNGQUANT_PATH'),
            '--quality',
            $quality,
            $file->getPathName(),
            '--output',
            $outputPath
        ];
        return ProcessService::handle($arguments) ? $outputFile : null;
    }
}