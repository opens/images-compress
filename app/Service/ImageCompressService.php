<?php

namespace App\Service;

use App\Service\gifsicle\GifsicleService;
use App\Service\jpegoptim\JpegoptimService;
use App\Service\pngquant\PngquantService;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Uuid;

class ImageCompressService
{
    public static function handle(array $files, int $quality = 80): array
    {
        $compressedFiles = [];
        foreach ($files as $file) {

            $ext = $file->getClientOriginalExtension();
            $fromPath = $file->move(storage_path('files/' . Session::getId()), Uuid::uuid4() . '.' . $ext);

            switch (strtolower($ext)) {
                case 'jpeg':
                case 'jpg':
                    $compressedFile = JpegoptimService::handle($fromPath, $quality);
                    break;
                case 'png':
                    $compressedFile = PngquantService::handle($fromPath, $quality);
                    break;
                case 'gif':
                    $compressedFile = GifsicleService::handle($fromPath, $quality);
                    break;
            }

            if ($compressedFile !== null) {
                $compressedFiles[$compressedFile] = $file->getClientOriginalName();
            }
        }
        return $compressedFiles;
    }
}