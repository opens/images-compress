<?php

namespace App\Service\gifsicle;

use App\Service\ProcessService;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\File;

class GifsicleService
{
    public static function handle(File $file, int $quality = 80): string|null
    {
        $outputFile = Uuid::uuid4() . '.' . $file->getExtension();
        $outputPath = storage_path('files/' . Session::getId() . '/' . $outputFile);
        $arguments = [
            env('GIFSICLE_PATH'),
            '-i',
            $file->getPathName(),
            '-O3',
            '--colors',
            '256',
            '-o',
            $outputPath
        ];
        return ProcessService::handle($arguments) ? $outputFile : null;
    }
}