<?php

namespace Tests\Feature;

use App\Service\ImageCompressService;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ImageCompressTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_file_compress()
    {
        $sessionId = 'Cl3TO8acqY9WXyLu3xp2ca1uiVp0ddp8Byjm4bY6';
        Session::setId($sessionId);
        Storage::disk('files')->deleteDirectory($sessionId);
        if (File::ensureDirectoryExists(base_path('tests/tmp'))) {
            File::makeDirectory(base_path('tests/tmp'));
        }
        $filesInput = [
            '1.png',
            '1.gif',
            '1.jpg'
        ];
        $files = [];
        foreach ($filesInput as $file) {
            File::copy('tests/files/' . $file, 'tests/tmp/' . $file);
            $files[] = new UploadedFile(base_path('tests/tmp/' . $file), $file, File::mimeType(base_path('tests/tmp/' . $file)), null, true);
        }

        $compressedFiles = ImageCompressService::handle($files, 70);

        foreach ($compressedFiles as $k => $file) {
            $beforeSize = File::size(base_path('tests/files/' . $file));
            $afterSize = Storage::disk('files')->size(Session::getId() . '/' . $k);
            $this->assertLessThan($beforeSize, $afterSize);
        }
    }
}
